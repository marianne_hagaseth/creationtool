package dom.regulation;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.isis.applib.AbstractService;
import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainService;


@DomainService
public class RESTclientTest extends AbstractService   {

	// From https://docs.jboss.org/resteasy/docs/3.0-beta-3/userguide/html/RESTEasy_Client_Framework.html
	
	@Action
	public String restClient() {
	 
		/**
		 * Conceptually, the steps required to submit a request are the following:
    1)obtain an Client instance
    2)create a WebTarget pointing at a Web resource
    3)build a request
    4)submit a request to directly retrieve a response or get a prepared Invocation for later submission
		 */
		
		try {
		
   //	 Client client = ClientBuilder.newBuilder().build();
	 
	// Create a client instance:
	Client client = ClientBuilder.newClient();
	
	// Create a WebTarget:::
	String resourceString = "192.168.33.10:9000/";
	WebTarget target = client.target(resourceString);
	 
	 // Build a request:
	Invocation.Builder invocationBuilder =
			target.request(MediaType.TEXT_HTML);
     Response response = invocationBuilder.get();
     
     if (response.getStatus() != 200) {
			throw new RuntimeException("" + response.getStatus());
		}
     
     String value = response.readEntity(String.class);
     response.close();  // You should close connections!
	
     System.out.println(value);
	
     return value;	
		
     	} catch (Exception e) {
			System.out.println("status:" + e.getMessage());
			if (e.getMessage().equals("404")) {
				System.out.println("Resource not found.");
			}  
			if (e.getMessage().equals("500")) {
				System.out.println("An unknown error occured.");
			}
			return "Exception Catched in RESTclientTest";
		}

		
	}
 
}
